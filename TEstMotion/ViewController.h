//
//  ViewController.h
//  TEstMotion
//
//  Created by zumry on 6/21/16.
//  Copyright © 2016 zumry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end

