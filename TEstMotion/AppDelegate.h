//
//  AppDelegate.h
//  TEstMotion
//
//  Created by zumry on 6/21/16.
//  Copyright © 2016 zumry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

