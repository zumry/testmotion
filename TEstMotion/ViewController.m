//
//  ViewController.m
//  TEstMotion
//
//  Created by zumry on 6/21/16.
//  Copyright © 2016 zumry. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *images = @[@"1.jpg",@"2.jpg", @"3.jpg", @"4.jpg", @"5.jpg", @"6.jpg"];
    
    for (int i = 0; i < 4; i++) {
        
        UIScrollView *view1 = [[UIScrollView alloc] initWithFrame: CGRectMake(i * self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
        if ((i % 2) != 0) {
            [view1  setBackgroundColor: [UIColor greenColor]];
            NSLog(@"divide by zero ttrue");
            
        } else {
            [view1  setBackgroundColor: [UIColor redColor]];
            NSLog(@"divide by zero false");
        }
        
        for (int x = 0; x < [images count]; x++) {
            
            CGFloat xOrigin = x * self.view.frame.size.height;
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, xOrigin, self.view.frame.size.width, self.view.frame.size.height)];
            
            imageView.image = [UIImage imageNamed: images[x]];
            
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            
            [view1 addSubview: imageView];
                       
        }
        
        view1.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height * [images count]);
        
        [self.scrollView addSubview: view1];
    }
    
    
    [self.scrollView setContentSize: CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height)];
     
    [self.scrollView scrollRectToVisible:CGRectMake(0, 200, self.scrollView.frame.size.width, self.scrollView.frame.size.height) animated: true];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    NSLog(@"page is %f ", page);
   // self.thePageControl.currentPage = page;
}

@end
