//
//  main.m
//  TEstMotion
//
//  Created by zumry on 6/21/16.
//  Copyright © 2016 zumry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
